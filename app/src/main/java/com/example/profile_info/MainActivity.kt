package com.example.profile_info

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity(){

    private lateinit var saveButton: Button
    private lateinit var clearButton: Button
    private lateinit var emailEditText: EditText
    private lateinit var usernameEditText: EditText
    private lateinit var firstNameEditText: EditText
    private lateinit var lastNameEditText: EditText
    private lateinit var ageEditText: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

    }
    private fun init() {
        saveButton = findViewById(R.id.save)
        clearButton = findViewById(R.id.clear)
        emailEditText = findViewById(R.id.email)
        usernameEditText = findViewById(R.id.username)
        firstNameEditText = findViewById(R.id.first_name)
        lastNameEditText = findViewById(R.id.last_name)
        ageEditText = findViewById(R.id.age)

        saveButton.setOnClickListener {
            check()
        }
        clearButton.setOnLongClickListener{
            clear()
            true
        }
    }
    private fun check(){
        val email:String = emailEditText.text.toString()
        val username:String = usernameEditText.text.toString()
        val firstName: String = firstNameEditText.text.toString()
        val lastName: String = lastNameEditText.text.toString()
        val age: String = ageEditText.text.toString()


        if(email.isEmpty() or username.isEmpty() or firstName.isEmpty() or lastName.isEmpty() or age.isEmpty()){
            Toast.makeText(this, "fill", Toast.LENGTH_SHORT).show()
            return
        }
        if ("@" !in email) {
            Toast.makeText(this, "Not valid e-mail", Toast.LENGTH_SHORT).show()
            return
        }
        if (username.length < 10) {
            Toast.makeText(this, "username should be more than 10", Toast.LENGTH_LONG).show()
            return
        }
        if (age.toInt() < 0) {
            Toast.makeText(this, "Not right age", Toast.LENGTH_SHORT).show()
            return
        }
    }
    private fun clear(){
        val email : EditText = emailEditText
        val firstName : EditText = emailEditText
        val lastName : EditText = emailEditText
        val username : EditText = emailEditText
        val age : EditText = emailEditText

        email.setText("")
        username.setText("")
        firstName.setText("")
        lastName.setText("")
        age.setText("")

        Toast.makeText(this,"Cleared", Toast.LENGTH_SHORT).show()


    }
}